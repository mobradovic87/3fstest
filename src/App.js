import React, { Component } from 'react';

import createBrowserHistory from 'history/createBrowserHistory'
import {
  Router ,
  Route } from 'react-router-dom';

import { NavbarContainer } from './components/Navbar';
import { BucketListContainer } from './components/BucketList';
import { BucketObjectComponent} from './components/BucketObjectsComponent';
import './App.css';

const history = createBrowserHistory();


class App extends Component {
  render() {
    return (

        <div>
          <NavbarContainer />
          <Router history={history}>
            <div>
              <Route exact path='/' component={BucketListContainer} />
              <Route path='/bucketObject/:id' component={BucketObjectComponent} />
           </div>
          </Router>
        </div>
    );
  }
}

export default App;
