import React, { Component } from 'react';
import {Button} from 'reactstrap';
import { Header } from '../common';
import { Link } from 'react-router-dom'
import BootstrapTable from 'react-bootstrap-table-next';
import './BucketListContainer.css';
import { CreateNewBucketComponent } from '../CreateNewBucketComponent';
import axios from 'axios';
import {BASE_URL,TOKEN} from '../../constants';
import {ModalComponent} from '../modalsComponent';


class BucketListContainer extends Component {

    constructor() {
        super();
        this.state = {
            showCreateForm:false,
            buckets:[],
            columns:[
                {
                    dataField: 'name',
                    text: 'Name',
                    formatter: (item)=>{
                        return <Link to={'/bucketObject/'+item.replace(/ /g,"-").toLowerCase()}>{item}</Link>
                    }
                },
                {
                    dataField: 'location',
                    text: 'Location'
                }
            ],
            ErrorText:'',
            modalTitle:'',
        };
        this.handleList = this.handleList.bind(this);
        this.HandleErrorCallback = this.HandleErrorCallback.bind(this);

    }
    HandleErrorCallback(){
        this.setState({
            ErrorText:'',
            modalTitle:'',
            showModal:false
        })
    }
    componentDidMount(){

       this.getBucketList();
    }

    getBucketList(){
        axios.get(BASE_URL+ "/buckets", {
            headers: {"Authorization": TOKEN}
        }).then(response=>{
            let buckets = response.data.buckets.map((bucket,key)=>{
                return {id:bucket.id,name:bucket.name, location:bucket.location.name};
            });
            this.setState({buckets:buckets});

        }).catch(err=>{
            this.setState({
                ErrorText:err.response.data.message,
                showModal:true,
                modalTitle:'Error'
            })
        });
    }
    show(){
       this.setState({showCreateForm:!this.state.showCreateForm})
    }

    handleList(isCreated) {
        this.setState({showCreateForm:false});
        this.getBucketList();
    }
    render(){
        return(
            <div className="container">
                <Header title="Bucket list" />
                { this.state.showCreateForm ?  <CreateNewBucketComponent
                    onCrateNewBucketList={this.handleList}/> : null }

            <main  className="bucketList--container">

                <div>
                    <div className="table-header">
                        <p className="float-left">All Buckets ({this.state.buckets.length})</p>
                        {!this.state.showCreateForm ?  <Button className="float-right"
                                                               color="primary" size="sm"
                                                               onClick={() => this.show() }>
                            Create new Bucket
                        </Button> : null}

                    </div>
                    <BootstrapTable
                        keyField='id'
                        data={ this.state.buckets }
                        columns={ this.state.columns }
                        bordered={ false }/>

                </div>
            </main>
                <ModalComponent  modalTitle={this.state.modalTitle}
                                 showModal={this.state.showModal}
                                 bodyText={this.state.ErrorText}
                                 buttonLabel={this.state.buttonText}
                                 showConfirmationModal={false}
                                 handleConfirm={this.HandleErrorCallback}
                                 disableButton={true}/>
            </div>
        )
    }

}

export default BucketListContainer;