import React, { Component } from 'react';
import { Button,Container, Row, Col,Input,Label } from 'reactstrap';
import './CreateNewBucketComponent.css';
import axios from 'axios';
import {BASE_URL,TOKEN} from '../../constants';
import {ModalComponent} from '../modalsComponent';
class CreateNewBucketComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            locations:[],
            bucketName:'',
            selectedLocation:'',
            ErrorText:'',
            modalTitle:'',

        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.HandleErrorCallback = this.HandleErrorCallback.bind(this);
    }
    componentWillUpdate(nextProps, nextState){
        console.log(nextProps);
        console.log(nextState);
    }

    componentDidMount() {
        axios.get(BASE_URL + "/locations", {
            headers: {"Authorization": TOKEN}
        }).then(response=>{
            this.setState({locations:response.data.locations});
            this.setState({selectedLocation:response.data.locations[0].id});
        }).catch(error=>{
            this.setState({
                ErrorText:error.response.data.message,
                showModal:true,
                modalTitle:'Error'
            })
        });
    }

    HandleErrorCallback(){
        this.setState({
            ErrorText:'',
            modalTitle:'',
            showModal:false
        })
    }
    handleInputChange(event) {
        const target = event.target;
        const name = target.name;
        const value =  target.value;
        this.setState({
            [name]: value
        });
    }

    CreateNewBucket() {
        axios.post(BASE_URL + "/buckets",{
            "name": this.state.bucketName,
            "location": this.state.selectedLocation
        }, {
            headers: {"Authorization": TOKEN}
        }).then(response=>{

            this.props.onCrateNewBucketList(true);

        }) .catch( (error)=> {
            this.setState({
                ErrorText:error.response.data.message,
                showModal:true,
                modalTitle:'Error'
            })

        });
    }
    render(){

        let locationsOptions = this.state.locations.map((city,key) =>{
           return <option key={key} value={city.id}>{city.name}</option>
        });
        return(
            <div className="form-container">
                <p >Create new</p>
                <main>
                    <Container>
                        <Row>
                            <Col xs="6">
                                <Label for="exampleText">Bucket Name*</Label>
                                <Input name="bucketName"  onChange={this.handleInputChange} id="bucketName"  />
                            </Col>
                            <Col xs="6">
                                <Label for="exampleText">Bucket Locations*</Label>
                                <Input  type="select" value={this.state.selectedLocation}  name="selectedLocation" onChange={this.handleInputChange}>
                                    {locationsOptions}
                                </Input>
                            </Col>
                        </Row>
                    </Container>
                    <Button type="button"
                            className="create-new-bucket--button"
                            color="primary"
                            size="sm"
                            onClick={() => this.CreateNewBucket()}
                            disabled={!this.state.bucketName}
                    >Create new Bucket</Button>
                </main>
                <ModalComponent  modalTitle={this.state.modalTitle}
                                 showModal={this.state.showModal}
                                 bodyText={this.state.ErrorText}
                                 buttonLabel={this.state.buttonText}
                                 showConfirmationModal={false}
                                 handleConfirm={this.HandleErrorCallback}
                                 disableButton={true}/>
            </div>
        )
    }
}

export default CreateNewBucketComponent;