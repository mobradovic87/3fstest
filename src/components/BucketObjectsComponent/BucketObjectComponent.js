import React, { Component } from 'react';
import {Header} from '../common/'
import {Button, TabContent, TabPane, Nav, NavItem, NavLink} from 'reactstrap';
import classnames from 'classnames';
import BootstrapTable from 'react-bootstrap-table-next';
import moment from 'moment';
import axios from 'axios';
import {BASE_URL,TOKEN} from '../../constants';
import {ModalComponent} from '../modalsComponent';
import './BucketObjectComponent.css';
class BucketObjectComponent extends Component {

    constructor(props) {

        super(props);
        this.state = {
            activeTab: '1',
            items:[],
            bucket:{},
            showConfirmModal:false,
            bucketLocation:'',
            selectedObject:null,
            showModal:false,
            storageSize:0,
            buttonText:'Delete',
            ModalBodyText:'',
            disabledButton:true,
            columns:[
                {
                    dataField: 'name',
                    text: 'Name',
                }, {
                    dataField: 'last_modified',
                    text: 'Last modified',
                    formatter: (item)=>{
                        return moment(item).format('DD.MM.YYYY');
                    }
                }, {
                    dataField: 'size',
                    text: 'Size',
                    formatter: (item) => {

                        return item / 1000000 + ' MB';
                    }
                }
            ],
            selectRow : {
                mode: 'radio',
                clickToSelect: true,
                hideSelectColumn: true,
                bgColor: '#6c757d',

                onSelect:(row, isSelect, rowIndex, e) => {

                   this.setState({
                       selectedObject:row.name,
                       disabledButton:false
                   })
                },


            },



        };
        this.toggle = this.toggle.bind(this);
        this.handleUploadChange = this.handleUploadChange.bind(this);
        this.handleSelectedItem = this.handleSelectedItem.bind(this);
        this.calculateSize = this.calculateSize.bind(this);
        this.handleModalCallback = this.handleModalCallback.bind(this);
        this.resetModals = this.resetModals.bind(this);
        this.HandleErrorCallback = this.HandleErrorCallback.bind(this);
        //this.deleteAllObjets = this.deleteAllObjets.bind(this);

    }

    setSelectedItem(name){
        this.setState({
            selectedObject: name,
            disabledButton: false
        });
    }
    componentDidMount(){
        this.getBucket();
        this.getObjects();
        this.resetModals();
    }

    getBucket() {
        axios.get(BASE_URL + "/buckets/"+this.props.match.params.id, {
            headers: {"Authorization": TOKEN}
        }).then(response=>{
            this.setState({bucket:response.data.bucket});
            this.setState({bucketLocation:response.data.bucket.location.name});
        }).catch((error) => {
            this.setState({
                ErrorText:error.response.data.message,
                showModal:true,
                modalTitle:'Error'
            });
        });
    }

    getObjects(){
        axios.get(BASE_URL + "/buckets/"+this.props.match.params.id+"/objects", {
            headers: {"Authorization": TOKEN}
        }).then(response=>{
            this.setState({items:response.data.objects});
            this.calculateSize(response.data.objects);
        }).catch( (error)=> {
            this.setState({
                ErrorText:error.response.data.message,
                showModal:true,
                modalTitle:'Error'
            })
        });

    }
    calculateSize(objects) {
        let size =0;
        objects.forEach(object=>{
            size += object.size /1000000;
        });
        this.setState({storageSize:size});
    }
    toggle(tab) {

        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    callModal(type){

        let text = '';
        switch (type)
        {
            case 'DeleteBucket':
                text='Delete Bucket : ' +this.state.bucket.name;
                break;
            case 'DeleteObject':
                text='Delete Bucket : ' +this.state.selectedObject;
                break;

        }
        this.setState({
            ModalBodyText:text,
            showConfirmModal:true,
            modalTitle:'Info',
            modalType:type
        });
    }

    resetModals(){
        this.setState({
            showConfirmModal:false,
            showModal:false,
            modalTitle:'',
            modalType:'',
            ModalBodyText:'',
        });
    }
    HandleErrorCallback() {
        this.resetModals();
    }
    handleModalCallback(confirm){
        this.resetModals();
        if(confirm){
            switch (this.state.modalType)
            {
                case 'DeleteBucket':
                    this.deleteBucket();
                    break;
                case 'DeleteObject':
                    this.deleteObject();
                    break;
            }
        }
    }
    deleteAllObjects(){

        for(let i=0;i <this.state.items.length;i++)
        {


            axios.delete(BASE_URL+'/buckets/'+this.props.match.params.id+'/objects/'+this.state.items[i].name,
                    {
                        params:{
                            bucket:this.props.match.params.id,
                            object:this.state.selectedObject
                        },
                        headers:{
                            'Authorization':TOKEN
                        }
                    }
                ).then(response=>{
                    this.setState({
                        selectedObject:null,
                        disabledButton:true
                    });
                    this.getObjects();
                }).catch(err=>{
                    this.setState({
                        ErrorText:err.response.data.message,
                        showModal:true,
                        modalTitle:'Error'
                    });

                });

            if(this.state.showModal)
            {
                break;
            }
        }

    }
    deleteBucket() {

            this.deleteAllObjects();
            if(!this.state.showModal){
            axios.delete(BASE_URL+'/buckets/'+this.props.match.params.id,
                {
                    params:{bucket:this.props.match.params.id},
                    headers:{
                        'Authorization':TOKEN
                    }
                }
            ).then(response=>{
               window.location.href ='/';
            }).catch(err=>{
                this.setState({
                    ErrorText:err.response.data.message,
                    showModal:true,
                    modalTitle:'Error'
                })
            });
        }
    }

    deleteObject(){

        axios.delete(BASE_URL+'/buckets/'+this.props.match.params.id+'/objects/'+this.state.selectedObject,
            {
                params:{
                    bucket:this.props.match.params.id,
                    object:this.state.selectedObject
                },
                headers:{
                    'Authorization':TOKEN
                }
            }
        ).then(response=>{
                this.setState({
                selectedObject:null,
                disabledButton:true
            });
            this.getObjects();
        }).catch(err=>{
            this.setState({
                ErrorText:err.response.data.message,
                showModal:true,
                modalTitle:'Error'
            })
        });
    }
    handleUploadChange(){

        var formData = new FormData();
        var object = document.querySelector('#objectInput');
        formData.append("file", object.files[0]);
        axios.post(BASE_URL+'/buckets/'+this.props.match.params.id+'/objects', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization':TOKEN
            }
        }).then(response=>{
            this.getObjects();
        }).catch(err=>{
            this.setState({
                ErrorText:err.response.data.message,
                showModal:true,
                modalTitle:'Error'
            })
        });

    }

    /**
     * this function get selected item id from Table component
     * @param id
     */
    handleSelectedItem(name) {
       this.setState({
           selectedObject:name,
           disabledButton:false
       });
    }

    render(){

        return(

            <div className="container">


                <Header title="Bucket Objects" />
                <div className="float-right">


                {this.state.activeTab == 2 ? <Button type="button"
                        className="button"
                        color="danger"
                        size="sm"
                        onClick={() => this.callModal('DeleteBucket')}>Delete Bucket</Button> : null}


                </div>
                <Nav tabs>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '1' })}
                            onClick={() => { this.toggle('1'); }}>
                            Files
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '2' })}
                            onClick={() => { this.toggle('2'); }}>
                            Details
                        </NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <main className="bucketObject--container">
                            <Button className="float-right button"
                                    color="primary"
                                    size="sm"
                                    onClick={(e) => this.objectInput.click()}>
                                Upload Object
                            </Button>
                            <Button type="button"
                                    className="float-right button"
                                    color="danger"
                                    size="sm"
                                    onClick={() => this.callModal('DeleteObject')}
                                    disabled={this.state.disabledButton}>
                                Delete Object
                            </Button>


                            <input id="objectInput" type="file" onChange={this.handleUploadChange} ref={(ref) => this.objectInput = ref} style={{ display: 'none' }} />

                            <BootstrapTable
                                keyField='name'
                                data={ this.state.items }
                                columns={ this.state.columns }
                                bordered={ false }
                                hover
                                selectRow={ this.state.selectRow}

                            />
                        </main>
                    </TabPane>
                    <TabPane tabId="2">
                        <main  className="bucketObject--container">

                            <p>
                                <span className="details--header">Bucket Name:</span><span className="details--text">{this.state.bucket.name}</span>
                            </p>
                            <p>
                                <span className="details--header">Location:</span><span className="details--text">{this.state.bucketLocation}</span>
                            </p>
                            <p>
                                <span className="details--header">Storage size:</span><span className="details--text"> {parseFloat(this.state.storageSize).toFixed(2)} MB</span>
                            </p>
                        </main>
                    </TabPane>
                </TabContent>
                <ModalComponent  modalTitle={'Bucket'}
                                 showModal={this.state.showConfirmModal}
                                 bodyText={this.state.ModalBodyText}
                                 handleConfirm={this.handleModalCallback}
                                 buttonLabel={this.state.buttonText}
                                 showConfirmationModal={true}
                                 />



                <ModalComponent  modalTitle={this.state.modalTitle}
                                 showModal={this.state.showModal}
                                 bodyText={this.state.ErrorText}
                                 buttonLabel={this.state.buttonText}
                                 handleConfirm={this.HandleErrorCallback}
                                 showConfirmationModal={false}
                                 />
            </div>
        )
    }
}
//"Delete "+ this.props.match.params.id+" Bucket?"}
export default BucketObjectComponent;