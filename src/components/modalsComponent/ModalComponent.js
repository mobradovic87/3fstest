import React, {Component} from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
class ModalComponent extends Component {

    constructor() {
        super();
        this.state = {
            modal: false,
            isButtonDisabled: false

        };
        this.toggle = this.toggle.bind(this);
        this.confirm = this.confirm.bind(this);
    }
    toggle() {
        this.setState({
            modal: !this.state.modal,
            isButtonDisabled:!this.state.isButtonDisabled
        });
    }
    confirm(){
        this.toggle();
        if(this.props.handleConfirm){
            this.props.handleConfirm(true);
        }
    }
    componentWillReceiveProps(nextProps) {
        // You don't have to do this check first, but it can help prevent an unneeded render
        this.setState({
            modal:nextProps.showModal,
            isButtonDisabled:nextProps.disableButton,
        })

    }
    render() {

        return (
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                <ModalHeader toggle={this.toggle}>{this.props.modalTitle}</ModalHeader>
                <ModalBody>
                    {this.props.bodyText}
                    </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => this.confirm()}>Confirm</Button>{' '}
                    {this.props.showConfirmationModal ? <Button color="secondary" onClick={this.toggle}>Cancel</Button> : null}
                </ModalFooter>
            </Modal>
        );
    }
}
export default ModalComponent;

