import React from 'react';
import './Header.css'
const Header = (props) => (
    <header>
        <h1 className='text-left'>{ props.title }</h1>
    </header>
)

export default Header;