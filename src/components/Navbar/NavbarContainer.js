import React  from 'react';

import {
    Navbar,
    NavbarBrand
} from 'reactstrap';

const NavbarContainer = () => (
    <Navbar color="light" light expand="md">
        <NavbarBrand href="/">Secure cloud storage</NavbarBrand>
    </Navbar>
);

export default NavbarContainer;